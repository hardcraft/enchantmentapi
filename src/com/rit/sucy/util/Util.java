package com.rit.sucy.util;

import java.math.BigDecimal;
import java.math.RoundingMode;

public class Util {

	public static double round(double value, int scale) {
	    if (scale < 0) throw new IllegalArgumentException();

	    BigDecimal bd = new BigDecimal(value);
	    bd = bd.setScale(scale, RoundingMode.HALF_UP);
	    return bd.doubleValue();
	}
	
}
